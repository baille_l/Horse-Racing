#!/usr/bin/env bash

echo "10 chevaux:"
./mouli/verif.sh 10
echo "100 chevaux:"
./mouli/verif.sh 100
echo "1000 chevaux:"
./mouli/verif.sh 1000
echo "10000 chevaux:"
./mouli/verif.sh 10000
