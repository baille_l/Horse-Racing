#!/usr/bin/env python3

import sys
import math

# Auto-generated code below aims at helping you parse
# the standard input according to the problem statement.

min = 10000000
pi = [int(line) for line in sys.stdin.read().split()]
n = pi.pop(0)
pi.sort()
for (i, v) in enumerate(pi):
    if i == 0:
        continue
    tmp = v - pi[i - 1]
    if (tmp < min):
        min = tmp
print(min)
                                    
